import sqlite3

#############Creating HOSPITAL Table##############

# Connect to sqlite database
h_conn = sqlite3.connect('hospital.db')
# cursor object
h_cursor = h_conn.cursor()
# drop query
h_cursor.execute('DROP TABLE IF EXISTS HOSPITAL')
# create query
h_query = """CREATE TABLE HOSPITAL(hospital_id INT PRIMARY KEY NOT NULL, hospital_name CHAR(20) NOT NULL,bed_count INT)"""
h_cursor.execute(h_query)
h_conn.commit()
h_conn.close()

###########################################

#############Inserting Into HOSPITAL Table##############
insert_conn = sqlite3.connect('hospital.db')
insert_conn.execute("INSERT INTO HOSPITAL (hospital_id,hospital_name,bed_count) "
             "VALUES (1, 'Mayo Clinic', '200')")
insert_conn.execute("INSERT INTO HOSPITAL (hospital_id,hospital_name,bed_count) "
             "VALUES (2, 'Cleveland Clinic', '400')")
insert_conn.execute("INSERT INTO HOSPITAL (hospital_id,hospital_name,bed_count) "
             "VALUES (3, 'Johns Hopkins', '1000')")
insert_conn.execute("INSERT INTO HOSPITAL (hospital_id,hospital_name,bed_count) "
             "VALUES (4, 'UCLA Medical Center', '1500')")

insert_cursor = insert_conn.cursor()
insert_cursor.execute('select * from HOSPITAL')
headings = [f"{id} \t\t{name} \t\t{n_beds}" for id,name,n_beds in insert_cursor.fetchall() ]
id,name,n_beds = 'Hospital_Id','Hospital_Name','Bed Count'
print('\n'.join([f"{id}\t{name}\t  {n_beds}"] + headings))
insert_conn.commit()
insert_conn.close()

###########################################

#############Creating DOCTOR Table##############

# Connect to sqlite database
d_conn = sqlite3.connect('doctor.db')
# cursor object
d_cursor = d_conn.cursor()
# drop query
d_cursor.execute('DROP TABLE IF EXISTS DOCTOR')
# create query
d_query = """CREATE TABLE DOCTOR(doctor_id INT PRIMARY KEY NOT NULL, doctor_name text,  hospital_id INT, joining_date text,speciality text, salary INT, experience INT,FOREIGN KEY(hospital_id) REFERENCES HOSPITAL(hospital_id))"""
d_cursor.execute(d_query)
d_conn.commit()
d_conn.close()

###########################################

print('\n'*5)

#############Inserting Into HOSPITAL Table##############
d_insert_conn = sqlite3.connect('doctor.db')
d_insert_conn.execute("INSERT INTO DOCTOR (doctor_id, doctor_name, hospital_id, joining_date,speciality,salary) "
             "VALUES (101, 'David',1,'2005-02-10', 'Pediatric',40000)")
d_insert_conn.execute("INSERT INTO DOCTOR (doctor_id, doctor_name, hospital_id, joining_date,speciality,salary) "
             "VALUES (102, 'Michael',1,'2018-07-23' ,'Oncologist',20000)")
d_insert_conn.execute("INSERT INTO DOCTOR (doctor_id, doctor_name, hospital_id, joining_date,speciality,salary) "
             "VALUES (103, 'Susan',2,'2016-05-19' ,'Garnacologist',25000)")
d_insert_conn.execute("INSERT INTO DOCTOR (doctor_id, doctor_name, hospital_id, joining_date,speciality,salary) "
             "VALUES (104, 'Robert',2,'2017-12-28' ,'Pediatric',28000)")
d_insert_conn.execute("INSERT INTO DOCTOR (doctor_id, doctor_name, hospital_id, joining_date,speciality,salary) "
             "VALUES (105, 'Linda',3,'2004-06-04' ,'Garnacologist',42000)")
d_insert_conn.execute("INSERT INTO DOCTOR (doctor_id, doctor_name, hospital_id, joining_date,speciality,salary) "
             "VALUES (106, 'William',3,'2012-09-11' ,'Dermatologist',30000)")
d_insert_conn.execute("INSERT INTO DOCTOR (doctor_id, doctor_name, hospital_id, joining_date,speciality,salary) "
             "VALUES (107, 'Richard',4,'2014-08-21' ,'Garnacologist',32000)")
d_insert_conn.execute("INSERT INTO DOCTOR (doctor_id, doctor_name, hospital_id, joining_date,speciality,salary) "
             "VALUES (108, 'Karen',4,'2011-10-17' ,'Radiologist',30000)")

d_insert_cursor = d_insert_conn.cursor()
d_insert_cursor.execute('select * from DOCTOR')
head = [f"{d_id}\t\t{d_name}\t\t{h_id}\t\t{j_date}\t\t{spec}\t\t{sal}\t\t{exp}" for d_id,d_name,h_id,j_date,spec,sal,exp in d_insert_cursor.fetchall()]
d_id,d_name,h_id,j_date,spec,sal,exp = 'Doctor_Id','Doctor_Name','Hospital_Id','Joining_Date','Speciality','Salary','Experience'
print('\n'.join([f"{d_id}  \t{d_name}  \t{h_id}  \t{j_date}  \t\t{spec}  \t\t{sal}  \t\t{exp}"] + head ))

d_insert_conn.commit()
d_insert_conn.close()




d_insert_conn.close()



