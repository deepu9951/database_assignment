import sqlite3

#############Creating Table##############

# Connect to sqlite database
conn = sqlite3.connect('car_owners.db')
# cursor object
cursor = conn.cursor()
# drop query
cursor.execute('DROP TABLE IF EXISTS CARS')
# create query
query = """CREATE TABLE CARS(car_name CHAR(20) NOT NULL, owner_name CHAR(20) NOT NULL)"""
cursor.execute(query)
conn.commit()
conn.close()

###########################################

#############Inserting Into Table##############

insert_conn = sqlite3.connect('car_owners.db')
insert_cursor = insert_conn.cursor()
#Addition of 10 Records according to user Choice
for i in range(10):
    name = input('Enter the Car Name ')
    owner = input('Enter Owner Name ')
    insert_query = """INSERT INTO CARS (car_name,owner_name) VALUES (?, ?)"""
    insert_cursor.execute(insert_query,(name,owner))

#Retrieval of Records
insert_cursor.execute('select * from CARS')
headings = [f"{car} {owner}" for car,owner in insert_cursor.fetchall()]
car,owner = 'CAR','OWNER'
print('\n'.join([f"{car} {owner}"] + headings))
insert_conn.commit()
insert_conn.close()

#################################################



