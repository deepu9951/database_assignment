import sqlite3

read_con1 = sqlite3.connect('doctor.db')
read_cursor1 = read_con1.cursor()
attachDatabaseSQL = "ATTACH DATABASE ? AS HP"
file_name = ("hospital.db",)
read_cursor1.execute(attachDatabaseSQL,file_name )
read_cursor2 = read_con1.cursor()
hosp_id = int(input('Enter the Hospital ID: '))
read_cursor2.execute(f"SELECT DOCTOR.doctor_name, HP.HOSPITAL.hospital_name FROM DOCTOR INNER JOIN HP.HOSPITAL ON DOCTOR.hospital_id = HP.HOSPITAL.hospital_id WHERE DOCTOR.hospital_id = {hosp_id};")
result = read_cursor2.fetchall()
print(result)
read_con1.commit()
read_con1.close()